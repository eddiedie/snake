// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"


class ASnakeElement;

UENUM()
enum class MoveDirection { UP,DOWN,LEFT,RIGHT };



UCLASS()
class SNAKE_API ASnakeBase : public AActor
{
	GENERATED_BODY()


	
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();


	UPROPERTY()
		ASnakeElement* SnakeElement;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElement> SnakeElementClass;
	UPROPERTY()
		TArray<ASnakeElement*> SnakeElementArray;

	UPROPERTY(EditDefaultsOnly) int		SnakeSize = 50;
	UPROPERTY(EditDefaultsOnly) int		SnakeNum = 5;
	UPROPERTY(EditDefaultsOnly) float	SnakeSpeed = 0.5;

	UPROPERTY(EditDefaultsOnly) MoveDirection LastMoveDirection = MoveDirection::DOWN;
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	void CreateSnakeArray(int Num);

	void MoveSnake();

	void SnakeElementOverlap(ASnakeElement* OverlappedElement, AActor* Other);

};
