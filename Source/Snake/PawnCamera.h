// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PawnCamera.generated.h"


class UCameraComponent;
class ASnakeBase;



UCLASS()
class SNAKE_API APawnCamera : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APawnCamera();

	UPROPERTY()
		UCameraComponent* MyCamera;

	UPROPERTY()
		ASnakeBase* SnakeBase;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase> SnakeBaseClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	void CreateSnakeBase();

	UFUNCTION()
		void PlayerInputVertical(float value);
	UFUNCTION()
		void PlayerInputHorizontal(float value);

};
