// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElement.h"
#include "SnakeInterface.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	CreateSnakeArray(SnakeNum);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorTickInterval(SnakeSpeed);
	MoveSnake();
}

void ASnakeBase::CreateSnakeArray(int Num)
{
	for (int i = 0; i < Num; i++)
	{
		FVector NewLocation(SnakeElementArray.Num() * SnakeSize, 0, 0);
		FTransform NewTransform(NewLocation);
		SnakeElement = GetWorld()->SpawnActor<ASnakeElement>(SnakeElementClass, NewTransform);
		SnakeElement->SnakeOwner = this;
		int32 ElementIndex = SnakeElementArray.Add(SnakeElement);

		if (ElementIndex == 0)
		{
			SnakeElement->SetHead();
		}
	}

	SnakeElement->SetActorHiddenInGame(true);
}


void ASnakeBase::MoveSnake()
{
	SnakeElement->SetActorHiddenInGame(false);
	FVector Direction(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case MoveDirection::UP:
		Direction.X += SnakeSize;
		break;

	case MoveDirection::DOWN:
		Direction.X -= SnakeSize;
		break;

	case MoveDirection::LEFT:
		Direction.Y -= SnakeSize;
		break;

	case MoveDirection::RIGHT:
		Direction.Y += SnakeSize;
		break;
	}


	SnakeElementArray[0]->ToggleCollision();
	
	for (int i = SnakeElementArray.Num()-1; i > 0; i--)
	{
		auto CurrentElement = SnakeElementArray[i];
		auto PrevElement = SnakeElementArray[i-1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElementArray[0]->AddActorWorldOffset(Direction);

	SnakeElementArray[0]->ToggleCollision();
}


void ASnakeBase::SnakeElementOverlap(ASnakeElement* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElementArray.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;

		auto SnakeInterface = Cast<ISnakeInterface>(Other);
		if (SnakeInterface)
		{
			SnakeInterface->SnakeInteraction(this, bIsFirst);
		}
	}
}
