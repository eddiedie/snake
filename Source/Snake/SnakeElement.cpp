// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElement.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"

// Sets default values
ASnakeElement::ASnakeElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComp"));
	RootComponent = StaticMeshComp;

}

// Called when the game starts or when spawned
void ASnakeElement::BeginPlay()
{
	Super::BeginPlay();
	StaticMeshComp->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElement::HandleBeginOverlap);
	StaticMeshComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	StaticMeshComp->SetCollisionResponseToAllChannels(ECR_Overlap);
	
}

// Called every frame
void ASnakeElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElement::SetHead_Implementation()
{
	
}


void ASnakeElement::SnakeInteraction(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		if (IsValid(SnakeOwner))
		{
			SnakeOwner->Destroy();
		}
	}
}

void ASnakeElement::ToggleCollision()
{
	if (StaticMeshComp->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		StaticMeshComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else
	{
		StaticMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}


void ASnakeElement::HandleBeginOverlap

(
	UPrimitiveComponent*		OverlappedComponent,
	AActor*						OtherActor,
	UPrimitiveComponent*		OtherComponent,
	int32						OtherBodyIndex,
	bool						bFromSweep,
	const FHitResult&			SweepResult
)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
	}
}
