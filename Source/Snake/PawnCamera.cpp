// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnCamera.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/InputComponent.h"

// Sets default values
APawnCamera::APawnCamera()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("MyCamera"));
	RootComponent = MyCamera;
	MyCamera->SetRelativeLocation(FVector(0, 0, 1000));
	MyCamera->SetRelativeRotation(FRotator(-90, 0, 0));
	

}

// Called when the game starts or when spawned
void APawnCamera::BeginPlay()
{
	Super::BeginPlay();
	CreateSnakeBase();
}

// Called every frame
void APawnCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APawnCamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APawnCamera::PlayerInputVertical);
	PlayerInputComponent->BindAxis("Horizontal", this, &APawnCamera::PlayerInputHorizontal);
}

void APawnCamera::CreateSnakeBase()
{
	SnakeBase = GetWorld()->SpawnActor<ASnakeBase>(SnakeBaseClass, FTransform());
}


void APawnCamera::PlayerInputVertical(float value)
{
	if (IsValid(SnakeBase))
	{
		if (value > 0 && SnakeBase->LastMoveDirection != MoveDirection::DOWN)
		{
			SnakeBase->LastMoveDirection = MoveDirection::UP;
		}
	}

	if (IsValid(SnakeBase))
	{
		if (value < 0 && SnakeBase->LastMoveDirection != MoveDirection::UP)
		{
			SnakeBase->LastMoveDirection = MoveDirection::DOWN;
		}
	}
}

void APawnCamera::PlayerInputHorizontal(float value)
{
	if (IsValid(SnakeBase))
	{
		if (value > 0 && SnakeBase->LastMoveDirection != MoveDirection::RIGHT)
		{
			SnakeBase->LastMoveDirection = MoveDirection::LEFT;
		}
	}

	if (IsValid(SnakeBase))
	{
		if (value < 0 && SnakeBase->LastMoveDirection != MoveDirection::LEFT)
		{
			SnakeBase->LastMoveDirection = MoveDirection::RIGHT;
		}
	}
}

